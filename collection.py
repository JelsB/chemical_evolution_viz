from species import Species
# set_default_slider_options,
# TESTcreate_interactive_slider_colormap_server)
from typing import List
from bokeh_functions import (rectangle_heatmaps_global_slider_server,
                             )


class Collection(object):
    """Collections of Species."""

    def __init__(self, species):
        self.species = {spec.name: spec for spec in species}

    def __repr__(self):
        return f'Col{[i for i in self.species.values()]}'

    def add_species(self, spec):
        if spec.name in self.species:
            print(f"Species {spec} is already part of the collection.")
        else:
            self.species[spec.name] = spec

    def create_all_interactive_slider_colormaps_server(self, **kwargs):
        """Create slider colormaps of all species."""
        for name, sp in self.species.items():
            sp.create_interactive_slider_colormap_server(**kwargs)

    def create_some_interactive_slider_colormaps_server(self, species_names,
                                                        **kwargs):
        """Create slider colormaps of some species."""
        for sp in species_names:
            try:
                self.species[sp].create_interactive_slider_colormap_server(**kwargs)
            except KeyError as err:
                print(f'{sp} is not part of the collection.'
                      f'Currently the collection is {self}')

    def heatmaps_with_global_slider(self, slider_options={}, figure_options={}):
        figs = []
        rects = []
        data_sources = []
        data_dicts = []
        for name, spec in self.species.items():
            p, r, data_source = spec.create_interactive_colormap(figure_options)
            figs.append(p)
            rects.append(r)
            data_sources.append(data_source)
            data_dicts.append(spec.model_grid)
        specs = {'fig': figs, 'rect': rects, 'data': data_sources}
        Species.set_default_slider_options(slider_options)

        var_name = Species.name_model_varying_var
        rectangle_heatmaps_global_slider_server(specs, data_dicts, var_name,
                                                slider_options)
