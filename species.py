import os
import pickle
import json
import pandas as pd
from collections import defaultdict
from bokeh_functions import (rectangle_one_heatmap_todoc,
                             rectangle_heatmaps_sliders_html,
                             rectangle_heatmaps_sliders_server,
                             rectangle_one_heatmap)


class Species(object):
    """docstring for Species."""

    range_slow_varying_var = []
    range_fast_varying_var = []
    range_model_varying_var = []

    max_slow_varying_var = None
    max_fast_varying_var = None
    max_model_varying_var = None

    delta_slow_varying_var = None
    delta_fast_varying_var = None
    delta_model_varying_var = None

    model_var_normaliser = None

    name_slow_varying_var = 'slow_var'
    name_fast_varying_var = 'fast_var'
    name_model_varying_var = 'model_var'

    units_slow_varying_var = 'slow_var_unit'
    units_fast_varying_var = 'fast_var_unit'
    units_model_varying_var = 'model_var_unit'

    default_figure_options = {'y_axis_label': f'{name_slow_varying_var}'
                              f' ({units_slow_varying_var})',
                              'x_axis_label': f'{name_fast_varying_var}'
                              f' ({units_fast_varying_var})',
                              }

    def __init__(self, name):
        self.name = name
        self.model_grid = defaultdict(list)
        # self.dataframes = {}
        self.max_abundance = 0
        self.min_abundance = 1e32

    def __repr__(self):
        return f'{self.name}'

    @classmethod
    def _get_cls_vars(cls):
        "Returns dictionary of all class variables."
        class_vars = dict()
        for key, val in vars(cls).items():
            if cls._is_cls_var(key, val):
                class_vars[key] = val
        return class_vars

    @classmethod
    def dump_cls_vars(cls, file):
        with file.open('w') as f:
            vars = cls._get_cls_vars()
            json.dump(vars, fp=f, indent=4)

    @staticmethod
    def _is_cls_var(name, val):
        """Checks if name, val combo is a class variable

        Retuns True if name, val combo is no magic function, no callable,
        no classmethod, and no staticmethod function."""
        return(not name.startswith('__') and not callable(val)
               and not isinstance(val, classmethod)
               and not isinstance(val, staticmethod))

    @classmethod
    def _read_in_cls_vars(cls, json_file):
        with json_file.open('r') as f:
            cls_vars = json.load(f)
        for var, value in cls_vars.items():
            # exec might not be the cleanest way but currently I cannot find
            # a better solution.
            try:
                exec(f'cls.{var} = {value}')
            except NameError as err:
                # Add Logger output here
                # execption when value is a string
                exec(f'cls.{var} = "{value}"')
            # except:
            #     print('someting when wrong with reading in class variables'
            #           f' {var} and {value}\n')
        return

    @classmethod
    def set_range_slow_varying_var(cls, range):
        cls.range_slow_varying_var = range
        cls.set_max_slow_varying_var()

    @classmethod
    def set_range_fast_varying_var(cls, range):
        cls.range_fast_varying_var = range
        cls.set_max_fast_varying_var()

    @classmethod
    def set_model_varying_var(cls, range):
        range = cls.convert_model_var(range)
        cls.range_model_varying_var = range
        cls.set_max_model_varying_var()

    @classmethod
    def append_range_slow_varying_var(cls, val):
        if val not in cls.range_slow_varying_var:
            cls.range_slow_varying_var.append(val)
            cls.set_max_slow_varying_var()

    @classmethod
    def append_range_fast_varying_var(cls, val):
        if val not in cls.range_fast_varying_var:
            cls.range_fast_varying_var.append(val)
            cls.set_max_fast_varying_var()

    @classmethod
    def append_model_varying_var(cls, val):
        val = cls.convert_model_var(val)
        if val not in cls.range_model_varying_var:
            cls.range_model_varying_var.append(val)
            cls.set_max_model_varying_var()

    @classmethod
    def set_max_slow_varying_var(cls):
        cls.max_slow_varying_var = max(cls.range_slow_varying_var)

    @classmethod
    def set_max_fast_varying_var(cls):
        cls.max_fast_varying_var = max(cls.range_fast_varying_var)

    @classmethod
    def set_max_model_varying_var(cls):
        cls.max_model_varying_var = max(cls.range_model_varying_var)

    @classmethod
    def convert_model_var(cls, val):
        """Convert model variable to more sensible values"""
        return int(val / cls.model_var_normaliser)

    @classmethod
    def map_model_var_value(cls, val):
        """Convert model variable to sensible strings"""
        return (f'{cls.name_model_varying_var}'
                f'{cls.convert_model_var(val)}')

    @classmethod
    def get_delta_fast_varying_var(cls):
        if not cls.delta_fast_varying_var:
            return (abs(cls.range_fast_varying_var[0]
                        - cls.range_fast_varying_var[1]))
        else:
            return cls.delta_fast_varying_var

    @classmethod
    def get_delta_slow_varying_var(cls):
        if not cls.delta_slow_varying_var:
            return (abs(cls.range_slow_varying_var[0]
                        - cls.range_slow_varying_var[1]))
        else:
            return cls.delta_slow_varying_var

    @classmethod
    def get_delta_model_varying_var(cls):
        if not cls.delta_model_varying_var:
            return (abs(cls.range_model_varying_var[0]
                        - cls.range_model_varying_var[1]))
        else:
            return cls.delta_model_varying_var

    @classmethod
    def create_minmax_range_dict(cls):
        delta_x = cls.get_delta_fast_varying_var()
        max_x = cls.max_fast_varying_var + delta_x
        delta_y = cls.get_delta_slow_varying_var()
        max_y = cls.max_slow_varying_var + delta_y

        uniq_xmin = cls.range_fast_varying_var
        uniq_xmax = cls.range_fast_varying_var[1:] + [max_x]
        uniq_ymin = cls.range_slow_varying_var
        uniq_ymax = cls.range_slow_varying_var[1:] + [max_y]

        xmin = [val for _ in range(len(uniq_ymin)) for val in uniq_xmin]
        xmax = [val for _ in range(len(uniq_ymax)) for val in uniq_xmax]
        ymin = [val for val in uniq_ymin for _ in range(len(uniq_xmin))]
        ymax = [val for val in uniq_ymax for _ in range(len(uniq_xmax))]

        return {f'{cls.name_fast_varying_var}_min': xmin,
                f'{cls.name_fast_varying_var}_max': xmax,
                f'{cls.name_slow_varying_var}_min': ymin,
                f'{cls.name_slow_varying_var}_max': ymax,
                }

    def update_model_grid_data(self, key, val):
        mapped_key = self.map_model_var_value(key)
        self.model_grid[mapped_key].append(val)
        self.update_max_abundance(val)
        self.update_min_abundance(val)

    def update_max_abundance(self, val):
        if val > self.max_abundance:
            self.max_abundance = val

    def update_min_abundance(self, val):
        if val < self.min_abundance:
            self.min_abundance = val

    def create_display_dict(self):
        minmax_ranges = self.create_minmax_range_dict()
        key_first_var = self.map_model_var_value(self.range_model_varying_var[0])
        start_model_var = {self.name_model_varying_var:
                           self.model_grid[key_first_var]}

        minmax_ranges.update(start_model_var)

        return minmax_ranges

    def create_interactive_colormap_old(self, model_var_val, model_var_idx=None,
                                        figure_options=None):
        if model_var_idx:
            model_var_val = self.range_model_varying_var[model_var_idx]

        minmax_ranges = self.create_minmax_range_dict()
        model_dic = {f'{model_var_val}': self.model_grid[model_var_val]}
        minmax_ranges.update(model_dic)
        df = pd.DataFrame(minmax_ranges)
        rectangle_one_heatmap_todoc(df, figure_options=figure_options)

    def create_interactive_colormap(self, figure_options={}):

        display_dict = self.create_display_dict()
        figure_options = self.update_figure_options(figure_options)

        p, r, data_source = rectangle_one_heatmap(display_dict, **figure_options)
        return p, r, data_source

    def create_interactive_slider_colormap_html(self, slider_options={},
                                                figure_options={}):

        display_dict = self.create_display_dict()
        display_dict.update(self.model_grid)

        slider_options = self.set_default_slider_options(slider_options)
        figure_options = self.update_figure_options(figure_options)
        # TODO: Get rid of the DataFrame, this is not necessary
        #       Change this is bokeh.functions.rectangle_heatmap_base
        df = pd.DataFrame(display_dict)
        rectangle_heatmaps_sliders_html(df, self.name_model_varying_var,
                                        slider_options=slider_options,
                                        figure_options=figure_options)

    def create_interactive_slider_colormap_server(self, slider_options={},
                                                  figure_options={}):

        display_dict = self.create_display_dict()
        # TODO: Must be better way of overwriting when not specified
        slider_options = self.set_default_slider_options(slider_options)
        figure_options = self.update_figure_options(figure_options)

        rectangle_heatmaps_sliders_server(display_dict, self.model_grid,
                                          self.name_model_varying_var,
                                          slider_options=slider_options,
                                          figure_options=figure_options)

    @classmethod
    def set_default_slider_options(cls, options, overwrite=False):
        if 'end' not in options or overwrite:
            options['end'] = cls.max_model_varying_var
        if 'step' not in options or overwrite:
            options['step'] = cls.get_delta_model_varying_var()

        return options

    @classmethod
    def set_default_figure_options(cls, options):
        # Note that this will override default settings if they are provided
        # in options
        cls.default_figure_options.update(options)

    @classmethod
    def update_figure_options(cls, options):
        # Note that this will override default settings if they are provided
        # in options
        return {**cls.default_figure_options, **options}

    def pickle_self(self, dir):
        with open(f'{dir}/{self.name}.pickle', 'wb') as file:
            pickle.dump(self, file, pickle.HIGHEST_PROTOCOL)
