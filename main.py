from pathlib import Path
from species import Species
from grid_models import ModelGrid
from collection import Collection
import sys

# Species.delta_slow_varying_var = abs(10**(-8.9) - 10**(-9))
# Species.delta_fast_varying_var = 10
Species.model_var_normaliser = 1. / (3.154e+7 / 60)

# gg = ModelGrid(input_file=Path("/home/jels/codes/krome_models/output/out_1d_small_dt/"
#                                "Al2O3_mono_upd.dat"))

gg = ModelGrid.from_pickles(Path('./data/'))
# gg.unpickle_species()
Species._read_in_cls_vars(Path('data/cls_vars.json'))

gg.pickle_species('./data')
# print(dir(Species))
# print(Species.__getattribute__)
# print(Species.__dict__)
# Species._read_in_cls_vars()
# Species.dump_cls_vars(Path('data/cls_vars.json'))
# print(Species._get_cls_vars())
test_species = gg.species['AL16O24']
test_species2 = gg.species['AL2O3']
test_species3 = gg.species['AL4O6']
all_spec = [test_species, test_species2, test_species3]
some_spec_names = ['AL16O24', 'AL3']
print(test_species.max_abundance)
sys.exit()

# test_species.create_interactive_colormap(model_var_val=2.8538813e-05)
# test_species.create_interactive_slider_colormap_html()
figure_options = dict(logticker=True, log_y=True, logcolor=True,
                      low_color=1, high_color=1e8)
slider_options = dict(title='time (min)')
# test_species.create_interactive_slider_colormap_server(
#     figure_options=figure_options, slider_options=slider_options)
# test_species2.create_interactive_slider_colormap_server(
#     figure_options=figure_options, slider_options=slider_options)
#
col = Collection(all_spec)
print(col.species)
# col.create_all_interactive_slider_colormaps_server(
#     figure_options=figure_options, slider_options=slider_options)
col.create_some_interactive_slider_colormaps_server(some_spec_names,
                                                    figure_options=figure_options,
                                                    slider_options=slider_options)
