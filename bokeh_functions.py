import os
import time
import pandas as pd
from bokeh.io import show
from bokeh.plotting import figure, output_file, save, curdoc
from bokeh.layouts import column, row
from bokeh.palettes import viridis
from bokeh.models import (LogColorMapper, LinearColorMapper, ColorBar,
                          BasicTicker, LogTicker, CustomJS,
                          ColumnDataSource, Slider)
from custom_bokeh_themes import light_minimal_copy


def rectangle_heatmap_base(df, savename='heatmap', log_y=False, logcolor=False,
                           logticker=False, low_color=None, high_color=None,
                           y_axis_label=None, x_axis_label=None, **kwargs):
    # get column names
    output_file(f'{savename}.html')

    xmin, xmax, ymin, ymax, z = list(df)[:5]
    data_source = ColumnDataSource(df)
    TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom"

    if not low_color:
        low_color = min(df[z])
    if not high_color:
        high_color = max(df[z])

    if not logcolor:
        mapper = LinearColorMapper(palette=viridis(256), low=low_color,
                                   high=high_color)
    else:
        mapper = LogColorMapper(palette=viridis(256), low=low_color,
                                high=high_color)
    if not logticker:
        ticker = BasicTicker()
    else:
        ticker = LogTicker()

    color_bar = ColorBar(color_mapper=mapper, ticker=ticker,
                         label_standoff=20, location=(0, 0))
    if log_y:
        y_axis_type = 'log'
    else:
        y_axis_type = 'auto'

    p = figure(x_range=(min(df[xmin]), max(df[xmax])),
               y_range=(min(df[ymin]), max(df[ymax])),
               x_axis_label=x_axis_label, y_axis_label=y_axis_label,
               x_axis_location="below", plot_width=900, plot_height=800,
               y_axis_type=y_axis_type, tools=TOOLS, toolbar_location='below',
               tooltips=[('Grid point', f'@{xmin}, @{ymin}'),
                         ('Abundance', f'@{z}')], **kwargs)

    r = p.quad(left=xmin, right=xmax, bottom=ymin, top=ymax, source=data_source,
               fill_color={'field': z, 'transform': mapper},
               line_color={'field': z, 'transform': mapper},
               # line_color=None,
               line_width=0)

    p.add_layout(color_bar, 'right')

    return p, r, data_source


def rectangle_heatmaps_sliders_html(df, var_name, slider_options=None,
                                    figure_options=None):
    """Create html of reactangular heatmap with slider

        Keys of the dataframe are expected to have the
        format: '<name><number>'
    """
    # TODO: decouple slider kwargs from **kwargs/add slider kwargs
    p, r, data_source = rectangle_heatmap_base(df, **figure_options)
    js_code = f"""
                var data = source.data;
                var idx = cb_obj.value;
                var name_idx = '{var_name}' + idx.toString();
                console.log(name_idx);
                data['{var_name}'] = data[name_idx];
                source.change.emit();
            """
    js_callback = CustomJS(args=dict(source=data_source), code=js_code)
    slider = one_slider(callback=js_callback, **slider_options)
    layout = column(p, slider)
    curdoc().add_root(layout)


def rectangle_one_heatmap_todoc(df, **kwargs):
    p, r, data_source = rectangle_heatmap_base(df, **kwargs)
    curdoc().add_root(p)
# TODO: remove discrepancy between both functions 'one_heatmap'


def rectangle_one_heatmap(display_dict, **figure_options):
    p, r, data_source = rectangle_heatmap_base(display_dict, **figure_options)
    return p, r, data_source


def rectangle_heatmaps_sliders_server(display_dict, data_dict, var_name,
                                      slider_options=None, figure_options=None):
    df = pd.DataFrame(display_dict)
    curdoc().theme = light_minimal_copy
    p, r, data_source = rectangle_heatmap_base(df, **figure_options)

    def update_slider(attrname, old, new):
        idx = f'{var_name}{int(slider.value)}'
        r.data_source.data[var_name] = data_dict[idx]
        # r.update(data_source=ColumnDataSource({idx: data_dict[idx]}))
        # print(r.data_source.data)

    slider = one_slider(**slider_options)
    slider.on_change('value', update_slider)
    layout = column(p, slider)
    curdoc().add_root(layout)


def rectangle_heatmaps_global_slider_server(specs_collection, data_dicts,
                                            var_name, slider_options=None):
    all_figs = specs_collection['fig']
    # for name, specs in specs_collection.items():
    curdoc().theme = light_minimal_copy

    def update_slider(attrname, old, new):
        idx = f'{var_name}{int(slider.value)}'
        for r, data_dict, data_source in zip(specs_collection['rect'],
                                             data_dicts,
                                             specs_collection['data']):

            r.data_source.data[var_name] = data_dict[idx]

    slider = one_slider(**slider_options)
    slider.on_change('value', update_slider)
    layout = column(slider, row(*all_figs))
    curdoc().add_root(layout)


def one_slider(start=0, end=1, step=1, value=0, **kwargs):
    return Slider(start=start, end=end, step=step, value=value, **kwargs)
