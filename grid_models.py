import pickle
from pathlib import Path
from species import Species


class ModelGrid(object):
    """Create grid of models.

        Each model corresponds to a (time, temperature, density)-combination
        with abundandes/number densities of each species as data.
        """

    line_delimiter = None
    num_gridpoints = 3
    idx_slow_varying_var = 2
    idx_fast_varying_var = 1
    idx_model_varying_var = 0

    def __init__(self, input_file=None, pickle_dir=None):
        self.species = dict()
        if input_file:
            self.input_file = Path(input_file)
            self.read_in_file(self.input_file)

        if pickle_dir:
            self.pickle_dir = Path(pickle_dir)
            self.unpickle_species(self.pickle_dir)

        else:
            print('Empty ModelGrid instance created')

    @classmethod
    def from_file(cls, input_file):
        "input file of all data of all species"
        return cls(input_file=input_file)

    @classmethod
    def from_pickles(cls, input_dir):
        "input dir of all pickled Species instances"
        return cls(pickle_dir=input_dir)

    @classmethod
    def set_line_delimiter(cls, delimiter):
        cls.line_delimiter = delimiter

    @classmethod
    def set_num_gridpoints(cls, num):
        cls.num_gridpoints = num

    @classmethod
    def set_idx_slow_varying_var(cls, idx):
        cls.idx_slow_varying_var = idx

    @classmethod
    def set_idx_fast_varying_var(cls, idx):
        cls.idx_fast_varying_var = idx

    @classmethod
    def set_idx_model_varying_var(cls, idx):
        cls.idx_model_varying_var = idx

    def clean_first_line(self, line):
        return line.strip().split(self.line_delimiter)[self.num_gridpoints:]

    def clean_data_line(self, line):
        cleaned_line = line.strip().split(self.line_delimiter)
        cleaned_line = [float(i) for i in cleaned_line]
        grid_points = cleaned_line[:self.num_gridpoints]
        data_points = cleaned_line[self.num_gridpoints:]
        return grid_points, data_points

    def update_species_data(self, data_list, model_var):
        """
        Update model grid dictionary of Species instances with data points.
        """
        for name, val in zip(self.species_names, data_list):
            self.species[name].update_model_grid_data(model_var, val)

    def update_species_cls_var_ranges(self, grid_points):
        model_var = grid_points[self.idx_model_varying_var]
        fast_var = grid_points[self.idx_fast_varying_var]
        slow_var = grid_points[self.idx_slow_varying_var]

        Species.append_model_varying_var(model_var)
        Species.append_range_fast_varying_var(fast_var)
        Species.append_range_slow_varying_var(slow_var)

    def create_species(self, file):

        first_line = True
        species = {}
        for line in file:
            if not line.strip():
                continue
            if not first_line:

                grid_points, data_points = self.clean_data_line(line)
                self.update_species_cls_var_ranges(grid_points)
                self.update_species_data(data_points,
                                         grid_points[self.idx_model_varying_var])

            else:
                self.species = {name: Species(name)
                                for name in self.clean_first_line(line)}
                self.species_names = [spec.name for spec in self.species.values()]
                first_line = False
        yield

    def read_in_file(self, file):
        with file.open('r') as f:
            list(self.create_species(f))

    def pickle_species(self, dir):
        for name, obj in self.species.items():
            obj.pickle_self(dir)

    def unpickle_species(self, pickle_dir):
        pickle_files = list(pickle_dir.glob('*.pickle'))
        for f in pickle_files:
            name = f.stem
            with f.open('rb') as file:
                obj = pickle.load(file)
            self.species[name] = obj
